function [xn, a_b, uin, Itg] = QrotorSim( xn, xc, a_b, ref, Itg, Ts, uin_r )
% Single Step Simulation of Quadrotor with Inner Loop Converter Only!
%       [xn1, a_b, uin] = QrotorSim( xn, xc, a_b, ref, Ts )
% xn1 = [pos; v_b; att; omg; xun] ----------- updated system state
% uin = [ail; ele; thr; rud] ---------------- current input to the inner loop 
% a_b1= [a_bx; a_by; a_bz] ------------------ updated body-axis acceleration m/s^2
% xn  = [pos; v_b; att; omg; xun; a_b] ------ current system state
%        pos = [x; y; z] -------------------- current ground-axis position, m
%        vel = [u; v; w] -------------------- current body-axis velocity, m/s
%        att = [phi; tht; psi] -------------- current attitude angles, rad
%        omg = [p; q; r] -------------------- current angular rates, rad/s
%        xun = [s_p; ss_p; s_q, ss_q] ------- current unmeasurable variables
% xc  = [pos; v_b; att; omg; xun] ----------- current measured system state for control 
%        pos = [x; y; z] -------------------- current ground-axis position, m
%        vel = [u; v; w] -------------------- current body-axis velocity, m/s
%        att = [phi; tht; psi] -------------- current attitude angles, rad
%        omg = [p; q; r] -------------------- current angular rates, rad/s
%        xun = [s_p; ss_p; s_q, ss_q] ------- current unmeasurable variables
% a_b = [a_bx; a_by; a_bz] ------------------ current body-axis acceleration m/s^2
% ref = [pos_r; vel_r; acc_r, psi_r; r_r] --- current reference input
%        pos_r ------------------------------ desired ground position, m/s
%        vel_r ------------------------------ desired ground velocity, m/s
%        acc_r ------------------------------ desired ground acceleration, m/s^2
%        psi_r ------------------------------ desired heading, rad
%        r_r   ------------------------------ desired heading rate, rad/s
% Itg = [ipos_err; ipsi_err]----------------- the integration error of the
%                                             position and  psi, temperory 
%                                             values
% psi and the reference
% Ts ---------------------------------------- sampling interval of control, sec


% Model parameters
g   = 9.781;        % gravitational acceleration, m/s^2
m   = 4.027;        % total mass of helicopter, kg




%% ----  The updated motor paramters for T-Lion on 17 Oct 2017  -----------
%  Suggested by Swee King and Yuchao


Kt1 = 0.00012551;       % Thrust coefficient, counter clock wise - Kt_ccw
Kt2 = 0.00013137;       % Thrust coefficient, clock wise         - Kt_cw
Kq  = 0.0000033;        % Torque coefficient                     - average Kq_cw, Kq_ccw

Kw1 = [566.6193 57.4600];     % the gain of PWM to rotation speed, counter clock wise. - pwm2omg_1st_ccw
Kw2 = [557.1556 57.9755];     % the gain of PWM to rotation speed, clock wise          - pwm2omg_1st_cw

Lm  = 0.303;        % the length of the arm of the platform, m

%% -------------- end of the update ---------------------------------------



Ixx = 0.0707;       % rolling moment of inertia, kg*m^2  (real measured value)
Iyy = 0.0705;       % pitching moment of inertia, kg*m^2 (ream measured value)
Izz = 0.2322;       % yawing moment of inertia, kg*m^2   (real measured value)



%% Inputs

pos_r = ref(1:3);
vel_r = ref(4:6);
acc_r = ref(7:9);
psi_r = ref(10);
r_r   = ref(11); 
psi_r = psi_r - floor( (psi_r+pi) / (2*pi) ) * (2*pi); % convert to [-pi pi]


%% Single Control Step, Multiple Simulation Steps
ts = 0.005;     % step size for model simulation, sec
Ts = max( ts, Ts );

%% Operating Point
vel0 = [0; 0; 0];
att0 = [0; 0; 0];
omg0 = [0; 0; 0];
xun0 = [0; 0; 0];
uin0 = [0; 0; 0; 0];
wnd0 = [0; 0; 0];


%% attitude control law
ksi   = 0.8; 
wn    = 2 * 1.55 * pi *3;  % 1.55 Hz bandwidth for the Blacklion 168
F_phi = -[wn^2 2*ksi*wn];
G_phi = -F_phi(1);
 
ksi   = 0.8; 
wn    = 2 * 1.55 * pi * 3;  % 1.55 Hz bandwidth for the Blacklion 168
F_tht = -[wn^2 2*ksi*wn];
G_tht = -F_tht(1);


%% xy outer-loop controller
wn    = 0.4;
sigma = 1.1;
ki    = 0.8;
eps   = 1 * 2;
F_xy  = [(wn^2+2*sigma*wn*ki)/eps^2, (2*sigma*wn+ki)/eps, -ki*wn^2/eps^3, -(wn^2+2*sigma*wn*ki)/eps^2, -(2*sigma*wn+ki)/eps];

%% z outer-loop controller
% wn    = 0.5;
wn    = 0.7;
sigma = 1.1;
eps   = 1;
F_z   = [(wn^2+2*sigma*wn*ki)/eps^2, (2*sigma*wn+ki)/eps, -ki*wn^2/eps^3, -(wn^2+2*sigma*wn*ki)/eps^2, -(2*sigma*wn+ki)/eps];

%% psi controller design
%wn    = 0.5;
%sigma = 1.2;
%eps   = 1*4;
wn    = 0.2;
sigma = 1.2;
eps   = 1*10;
F_c   = [2*wn*sigma/eps^2, -(wn^2)/eps, -2*wn*sigma/eps^2];


%% current states
pos = xc(1:3);
v_b = xc(4:6);
phi = xc(7) - floor( (xc(7)+pi) / (2*pi) ) * (2*pi);  % convert to [-pi pi]
tht = xc(8) - floor( (xc(8)+pi) / (2*pi) ) * (2*pi);
psi = xc(9) - floor( (xc(9)+pi) / (2*pi) ) * (2*pi);
omg = xc(10:12);
%xun = xc(13:15);

R = eye(3,3); % transformation from body coorindates to ground coordinates
R(1,1) = cos(psi)*cos(tht);
R(1,2) = cos(psi)*sin(tht)*sin(phi) - sin(psi)*cos(phi);
R(1,3) = cos(psi)*sin(tht)*cos(phi) + sin(psi)*sin(phi);
R(2,1) = sin(psi)*cos(tht);
R(2,2) = sin(psi)*sin(tht)*sin(phi) + cos(psi)*cos(phi);
R(2,3) = sin(psi)*sin(tht)*cos(phi) - cos(psi)*sin(phi);
R(3,1) =-sin(tht);
R(3,2) = cos(tht)*sin(phi);
R(3,3) = cos(tht)*cos(phi);
v_g    = R * v_b; 


%% outer-loop control
ipos_err = Itg(1:3) + Ts * ( pos_r - pos ); 

psi_err  = ( psi_r - psi ); % the error of psi_r and psi
psi_err  = psi_err - floor( (psi_err+pi) / (2*pi) ) * (2*pi); % convert to [-pi pi]
ipsi_err = Itg(4) + Ts * psi_err; 
ipsi_err = ipsi_err - floor( (ipsi_err+pi) / (2*pi) ) * (2*pi); % convert to [-pi pi]

Itg(1:3) = ipos_err; 
Itg(4)   = ipsi_err; 

%% xy 
agx_r    = [F_xy, 1] * [ pos_r(1); vel_r(1); ( -1 * ipos_err(1) ); pos(1); v_g(1); acc_r(1)]; 
agy_r    = [F_xy, 1] * [ pos_r(2); vel_r(2); ( -1 * ipos_err(2) ); pos(2); v_g(2); acc_r(2)]; 

thr_agx = 6; % threshold
thr_agy = 6; 

if (agx_r < -thr_agx) agx_r = -thr_agx; end
if (agx_r >  thr_agx) agx_r =  thr_agx; end
if (agy_r < -thr_agy) agy_r = -thr_agy; end
if (agy_r >  thr_agy) agy_r =  thr_agy; end


%% z
agz_r    = [F_z, 1] * [ pos_r(3); vel_r(3); ( -1 * ipos_err(3) ); pos(3); v_g(3); acc_r(3)]; 
a_g_r    = [agx_r; agy_r; agz_r];

thr_agz = 2; % threshold

if (agz_r < -thr_agz) agz_r = -thr_agz; end
if (agz_r >  thr_agz) agz_r =  thr_agz; end


%% psi
%dpsi_r   = [F_c,  1] * [ psi_r; ( ipsi-ipsi_r );  psi;  r_r]; 
%dpsi_r   = [F_c,  1] * [ psi_r; ( -ipsi_err );  psi;  r_r]; 

dpsi_r = F_c(1) * ( psi_err) + F_c(2) * (-1) * ipsi_err + r_r;

if (dpsi_r<-0.5) dpsi_r = -0.5; end
if (dpsi_r> 0.5) dpsi_r =  0.5; end


ksi   = 0.8; 
wn    = 2 * 0.5 * pi * 2;  % 1.55 Hz bandwidth for the Blacklion 168
F_psi = -[wn^2 2*ksi*wn];
G_psi = -F_psi(1);


% Command Conversion (using Wang Fei's method)
% dc_thr2w   = -5.7538;
% dc_ele2tht =  0.6151;
% dc_ail2phi =  0.6151;
% dc_rud2r   =  3.3721;
% 
% damping_u  =  0.09508;
% damping_v  =  0.09508;
% ratio_u    = -8.661;
% ratio_v    =  8.661;
% 
% ail  = ( v_b(2) * damping_v + a_b_r(2) ) / ratio_v / dc_ail2phi;
% ele  = ( v_b(1) * damping_u + a_b_r(1) ) / ratio_u / dc_ele2tht;
% thr  = wb_r / dc_thr2w;
% rud  = dpsi_r / dc_rud2r; 

R_psi  = [ cos(psi) sin(psi) 0; 
          -sin(psi) cos(psi) 0; 
                 0        0  1]; 
a_g_r2 = R_psi * ( a_g_r - [0; 0; g]); % convert the acceleration to the UAV heading coordinate system


abz_r  = a_g_r2(3) / ( cos(tht) * cos(phi) ); % calculate the reference for abz, phi, and theta
% tht_r  = asin( a_g_r2(1) / abz_r );
% phi_r  = asin( - a_g_r2(2) / ( abz_r * cos(tht_r)) );
phi_r = asin( -a_g_r2(2) / abz_r  );
tht_r = asin(  a_g_r2(1) / ( abz_r*cos(phi_r) )  );


%% For inner loop control
u_p = Ixx * ( F_phi * [phi; omg(1)] + G_phi * phi_r );
u_q = Iyy * ( F_tht * [tht; omg(2)] + G_tht * tht_r );
%u_r = Izz * ( F_psi * [psi; omg(3)] + G_psi * psi_r );
u_r = Izz * ( F_psi(1) * (-1) * psi_err + F_psi(2) * omg(3) );
%u_r = Izz * dpsi_r;
u_z = m * abz_r;

% u_p = uin_r(1);
% u_q = uin_r(2);
% u_r = uin_r(3);
% u_z = uin_r(4); 


%% ESC control
omega2_u = [ -Lm*Kt1/sqrt(2)  Lm*Kt1/sqrt(2)  Lm*Kt2/sqrt(2)  -Lm*Kt2/sqrt(2);
              Lm*Kt1/sqrt(2) -Lm*Kt1/sqrt(2)  Lm*Kt2/sqrt(2)  -Lm*Kt2/sqrt(2);
              Kq                         Kq             -Kq               -Kq;
             -Kt1                       -Kt1            -Kt2              -Kt2];      
u_omega2 = omega2_u^-1;

omega2 = u_omega2 * [u_p; u_q; u_r; u_z];   

if omega2(1) < 0 omega2(1)=0; end
if omega2(2) < 0 omega2(2)=0; end
if omega2(3) < 0 omega2(3)=0; end
if omega2(4) < 0 omega2(4)=0; end

omega  = sqrt(omega2);

%% calculate the PWM signal for the motors based on first order approximation
uin(1) = ( omega(1) - Kw1(2) ) / Kw1(1); 
uin(2) = ( omega(2) - Kw1(2) ) / Kw1(1);
uin(3) = ( omega(3) - Kw2(2) ) / Kw2(1);
uin(4) = ( omega(4) - Kw2(2) ) / Kw2(1);

%% check the range
if uin(1) <0
    uin(1) =0;
end

if uin(1) >1
    uin(1) = 1;
end

if uin(2) <0
    uin(2) =0;
end

if uin(2) >1
    uin(2) = 1;
end

if uin(3) <0
    uin(3) =0;
end

if uin(3) >1
    uin(3) = 1;
end

if uin(4) <0
    uin(4) =0;
end

if uin(4) >1
    uin(4) = 1;
end

%uin = [ail; ele; thr; rud];
%uin = uin_r;



% % current control
% a_g_c = Go * [ pos_r; vel_r; acc_r ] + Fo * [ pos; v_g ];
% a_b_c = R' * a_g_c;
% dlt_c = CC(1,:) * a_b_c;
% phi_c = CC(2,:) * a_b_c;
% tht_c = CC(3,:) * a_b_c;
% psi_c = psi_r - psi;
% psi_c = psi_c - floor( (psi_c+pi) / (2*pi) ) * (2*pi);
% yaw = -psi_c;
% dui = Gi * [phi_c; tht_c; psi_c] ...
%     + Fi * ( [phi; tht; yaw; omg; xun] - [att0; omg0; xun0] );
% uin = [dlt_c; dui] + uin0;
% uin = max( min( uin, 1 ), -1 );


uin = [0.5; 0.5; 0.5; 0.5];



%% state update
for k = 1:Ts/ts
    if k == 1
        [k1, a_b] = QrotorDynMod( xn, uin, [0;0;0] );
        %k1 = QrotorDynMod( xn, uin , [0;0;0] );
    else
        k1 = QrotorDynMod( xn, uin , [0;0;0] );
    end
    k2 = QrotorDynMod( xn+ts/2*k1, uin , [0;0;0] );
    k3 = QrotorDynMod( xn+ts/2*k2, uin , [0;0;0] );
    k4 = QrotorDynMod( xn+ts  *k3, uin , [0;0;0] );
    xn = xn + ts/6 * ( k1 + 2*k2 + 2*k3 + k4 );
    
    % calculate omg_r, since psi is modelled using a first order system 
    % xn(12) =  ( k1(9) + 2*k2(9) + 2*k3(9) + k4(9) ) / 6;
end


%% calculate a_b
xn(7) = xn(7) - floor( (xn(7)+pi) / (2*pi) ) * (2*pi);
xn(8) = xn(8) - floor( (xn(8)+pi) / (2*pi) ) * (2*pi);
xn(9) = xn(9) - floor( (xn(9)+pi) / (2*pi) ) * (2*pi);
phi = xn(7);
tht = xn(8);
psi = xn(9); 
psi = psi - floor( (psi+pi) / (2*pi) ) * (2*pi); % convert to [-pi pi]
xn(9) = psi; 

% phi = xn(7) - floor( (xn(7)+pi) / (2*pi) ) * (2*pi);
% tht = xn(8) - floor( (xn(8)+pi) / (2*pi) ) * (2*pi);
% psi = xn(9) - floor( (xn(9)+pi) / (2*pi) ) * (2*pi);

R(1,1) = cos(psi)*cos(tht);
R(1,2) = cos(psi)*sin(tht)*sin(phi) - sin(psi)*cos(phi);
R(1,3) = cos(psi)*sin(tht)*cos(phi) + sin(psi)*sin(phi);
R(2,1) = sin(psi)*cos(tht);
R(2,2) = sin(psi)*sin(tht)*sin(phi) + cos(psi)*cos(phi);
R(2,3) = sin(psi)*sin(tht)*cos(phi) - cos(psi)*sin(phi);
R(3,1) =-sin(tht);
R(3,2) = cos(tht)*sin(phi);
R(3,3) = cos(tht)*cos(phi);

a_b = R' * ( R * xn(4:6) - v_g ) / Ts; % differentiate ground velocity





function varargout = QrotorDynMod( x, u, wnd )
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%        Linear Dynamic Model of Quadrotor UAV                           %%%%
%%%%        Quadrotor with Naja attitude stability augmentation             %%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% [dx,a_b] = QrotorDynMod( x, u, wnd )
% x   = [pos; vel; att; omg; xun] ------- current system state
% pos = [x; y; z] ----------------------- current ground-axis position, m
% vel = [u; v; w] ----------------------- current body-axis velocity, m/s%
% att = [phi; theta; psi] --------------- current attitude angles, rad
% datt= [dphi; dtht, dpsi] -------------- current derivatives of attitude angles, rad/s
% xun = [d2phi; d3phi; d2tht; d3tht] ---- current 2nd and 3rd order derivatives of attitude angles
% u   = [lat; lon; thr; ped ] in [-1 1] without units
% wnd = [ wnd_u; wnd_v; wnd_w ] body-axis wind velocity as disturbance, m/s
% dx  delat of x
%
% Coded by Lin Feng    Modeled by Cui Jinqiang    Directed by Ben M. Chen
% Copyright 2013 research UAV team, National University of Singapore
% Updated: 28 Aug 2013

ts = 0.005;     % step size for model simulation, sec

persistent  g  m  Ixx  Iyy  Izz Lm;

%% Model paramters

%if isempty(g)
g   = 9.781;        % gravitational acceleration, m/s^2
m   = 4.027;        % total mass of helicopter, kg

rho = 1.292;        % air density, kg/m^3

Ixx = 0.0707;       % rolling moment of inertia, kg*m^2  (real measured value)
Iyy = 0.0705;       % pitching moment of inertia, kg*m^2 (scaled based on CIFER)
Izz = 0.2322;       % yawing moment of inertia, kg*m^2   (real measured value)

%% ---  The updated motor paramters for T-Lion on 17 Oct 2017  -----------
%  Suggested by Swee King and Yuchao

Lm  = 0.303;        % the length of the arm of the platform


Jr  = 0;            % rotor inertia
OMG_r = 0;          % rotation speed of the motor


%% Parse Function Inputs
u1 = u(1);          % input to the esc
u2 = u(2);
u3 = u(3);
u4 = u(4);

vel_u = x(4);       % m/s
vel_v = x(5);       % m/s
vel_w = x(6);       % m/s
phi   = x(7);       % rad
tht   = x(8);       % rad
psi   = x(9);       % rad
omg_p = x(10);      % rad/s
omg_q = x(11);      % rad/s
omg_r = x(12);      % rad/s
% b1s   = x(13);      % rad
% b1c   = x(14);      % rad
% r_c   = x(15);      % none

% u_a = vel_u - wnd(1);
% v_a = vel_v - wnd(2);
% w_a = vel_w - wnd(3);


%% the thrust of the rotors
%  
%  configuration of the uav   
% 
%   3(cw)   1(ccw)
%       \  / 
%        \/
%        /\   
%       /  \
%  2(ccw)   4(cw)



%% ---  The updated motor paramters for T-Lion on 17 Oct 2017  -----------
%  Suggested by Swee King and Yuchao


pwm2omg_ccw = [ 449.5054, -923.2888,  1058.7,    8.1576];
omg2Tz_ccw  = [ 0.00012551,   -0.0069,    0.1698];
omg2Rz_ccw  = [ 0.0000037357,  -0.00030354,  0.0095];

pwm2omg_cw  = [ 498.5041, -993.2777,  1075.2,    6.9775];
omg2Tz_cw   = [ 0.00013137,   -0.0077,    0.2126];
omg2Rz_cw   = [ 0.0000040001,  -0.00036572,  0.0112];




%% compute the thrust and torque

Omg1 = pwm2omg_ccw * [ u1^3; u1^2; u1; 1]; 
Omg2 = pwm2omg_ccw * [ u2^3; u2^2; u2; 1]; 
Omg3 = pwm2omg_cw  * [ u3^3; u3^2; u3; 1]; 
Omg4 = pwm2omg_cw  * [ u4^3; u4^2; u4; 1]; 

T_rt1 = omg2Tz_ccw * [ Omg1^2; Omg1; 1]; 
T_rt2 = omg2Tz_ccw * [ Omg2^2; Omg2; 1]; 
T_rt3 = omg2Tz_cw  * [ Omg3^2; Omg3; 1]; 
T_rt4 = omg2Tz_cw  * [ Omg4^2; Omg4; 1]; 

Q_rt1 = omg2Rz_ccw * [ Omg1^2; Omg1; 1];  
Q_rt2 = omg2Rz_ccw * [ Omg2^2; Omg2; 1];  
Q_rt3 = omg2Rz_cw  * [ Omg3^2; Omg3; 1];  
Q_rt4 = omg2Rz_cw  * [ Omg4^2; Omg4; 1];  


%% rotor force and moments
Xrt =  0;
Yrt =  0; 
Zrt = -(T_rt1 + T_rt2 + T_rt3 + T_rt4);

Lrt = Lm / sqrt(2) * ( -T_rt1 + T_rt2 + T_rt3 - T_rt4 );
Mrt = Lm / sqrt(2) * (  T_rt1 - T_rt2 + T_rt3 - T_rt4 );
Nrt = Q_rt1 + Q_rt2 - Q_rt3 - Q_rt4;



% Kt1 = 0.0005;       % Thrust coefficient, counter clock wise
% Kt2 = 0.0005;       % Thrust coefficient, clock wise
% Kq  = 0.000016;     % Drag coefficient
% 
% omega2_u = [ -Lm*Kt1/sqrt(2)  Lm*Kt1/sqrt(2)  Lm*Kt2/sqrt(2)  -Lm*Kt2/sqrt(2);
%               Lm*Kt1/sqrt(2) -Lm*Kt1/sqrt(2)  Lm*Kt2/sqrt(2)  -Lm*Kt2/sqrt(2);
%               Kq                         Kq             -Kq               -Kq;
%              -Kt1                       -Kt1            -Kt2              -Kt2];
%          
% U = omega2_u * [ Omg1^2; Omg2^2; Omg3^2; Omg4^2 ]; 
% Lrt = U(1);
% Mrt = U(2);
% Nrt = U(3);
% Zrt = U(4); 


%% resulted forces and moments along body axes
F_u = Xrt;
F_v = Yrt;
F_w = Zrt;
M_p = Lrt;
M_q = Mrt;
M_r = Nrt;


%% Rigid body dynamics
domg_p = ( ( Iyy - Izz ) * omg_q * omg_r + Jr * omg_q * OMG_r + M_p ) / Ixx; % body gyro effect + propeller gyro effect + roll actuator action
domg_q = ( ( Ixx - Izz ) * omg_p * omg_r + Jr * omg_p * OMG_r + M_q ) / Iyy;
domg_r = ( ( Ixx - Iyy ) * omg_p * omg_q + M_r) / Izz;

dphi = [1  sin(phi)*tan(tht)  cos(phi)*tan(tht)] * [omg_p; omg_q; omg_r];
dtht = [0       cos(phi)          -sin(phi)    ] * [omg_p; omg_q; omg_r];
dpsi = [0  sin(phi)*sec(tht)  cos(phi)*sec(tht)] * [omg_p; omg_q; omg_r];


R = eye(3,3); % rotation matrix transforming body-axis vector to ned-axis vector
R(1,1) = cos(psi)*cos(tht);
R(1,2) = cos(psi)*sin(tht)*sin(phi) - sin(psi)*cos(phi);
R(1,3) = cos(psi)*sin(tht)*cos(phi) + sin(psi)*sin(phi);
R(2,1) = sin(psi)*cos(tht);
R(2,2) = sin(psi)*sin(tht)*sin(phi) + cos(psi)*cos(phi);
R(2,3) = sin(psi)*sin(tht)*cos(phi) - cos(psi)*sin(phi);
R(3,1) = -sin(tht);
R(3,2) = cos(tht)*sin(phi);
R(3,3) = cos(tht)*cos(phi);

agx = R(1,:) * [ F_u; F_v; F_w; ] / m ;
agy = R(2,:) * [ F_u; F_v; F_w; ] / m;
agz = R(3,:) * [ F_u; F_v; F_w; ] / m + g;

a_u = F_u / m - g * sin(tht);
a_v = F_v / m + g * sin(phi)*cos(tht);
a_w = F_w / m + g * cos(phi)*cos(tht);

dvel_u = a_u - omg_q*vel_w + omg_r*vel_v;
dvel_v = a_v - omg_r*vel_u + omg_p*vel_w;
dvel_w = a_w - omg_p*vel_v + omg_q*vel_u;

v_g  = R * [vel_u; vel_v; vel_w];
dpos = v_g;

  
%% Funtion Outputs
dx = [dpos;             dvel_u; dvel_v; dvel_w;
      dphi; dtht; dpsi; domg_p; domg_q; domg_r;];
if nargout == 1
    varargout{1} = dx;
elseif nargout == 2
    varargout{1} = dx;
    varargout{2} = [F_u; F_v; F_w]/m;
end

